
singleton Material(Helper_01_WarnTexture)
{
   mapTo = "WarnTexture";
   diffuseMap[0] = "WarningTexture";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};
