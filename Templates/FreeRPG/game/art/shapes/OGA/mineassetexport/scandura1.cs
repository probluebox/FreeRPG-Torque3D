
singleton TSShapeConstructor(Scandura1Dae)
{
   baseShape = "./scandura1.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
