
singleton Material(gate2_grilaj)
{
   mapTo = "grilaj";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/balama1.png";
};

singleton Material(gate2_cadru_piatra)
{
   mapTo = "cadru_piatra";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/podea.png";
};

singleton Material(gate2_usa1_2)
{
   mapTo = "usa1-2";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/lemn_usa.png";
};

singleton Material(minable_ore_stanca1)
{
   mapTo = "unmapped_mat";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};


singleton Material(TerrainFX_stone)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Church/Stone.jpg";
   doubleSided = "1";
   showDust = "1";
   effectColor[0] = "0.25 0.25 0.25 1";
   effectColor[1] = "0.25 0.25 0.25 0";
   footstepSoundId = "1";
   impactSoundId = "1";
   terrainMaterials = "1";
   materialTag0 = "Terrain";
};

singleton Material(boss_house_usa_intrare__reboot2_toc_usa_stang)
{
   mapTo = "toc_usa_stang";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_toc_usa_drept)
{
   mapTo = "toc_usa_drept";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_scandura_oriz_2)
{
   mapTo = "scandura_oriz_2";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_scandura_oriz_1)
{
   mapTo = "scandura_oriz_1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_Piatra_cadru_usa)
{
   mapTo = "Piatra_cadru_usa";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_clanta_001)
{
   mapTo = "clanta_001";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_broasca)
{
   mapTo = "broasca";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_balama)
{
   mapTo = "balama";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(boss_house_usa_intrare__reboot2_Usa_body)
{
   mapTo = "Usa_body";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(ore1_base_stone1)
{
   mapTo = "base_stone1";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(ore1_ore_vein1)
{
   mapTo = "ore_vein1";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(campfire_Soup_campfire)
{
   mapTo = "Soup_campfire";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/cica coffee.jpg";
};

singleton Material(campfire_Rocks_campfire)
{
   mapTo = "Rocks_campfire";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/cauldron_rocks_diffus.tga";
   normalMap[0] = "art/shapes/OGA/mineassetexport/Texturi/cauldron_rocks_normal.tga";
};

singleton Material(campfire_Cauldron_handle)
{
   mapTo = "Cauldron_handle";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/balama1.png";
};

singleton Material(campfire_Cauldron)
{
   mapTo = "Cauldron";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   diffuseColor[1] = "White";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/balama1.png";
};

singleton Material(campfire_Ashes_campfire)
{
   mapTo = "Ashes_campfire";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/cauldron_ash_dffuse.tga";
   normalMap[0] = "art/shapes/OGA/mineassetexport/Texturi/cauldron_ash_normal.tga";
};

singleton Material(campfire_Suporti_metal_campfire)
{
   mapTo = "Suporti_metal_campfire";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/balama1.png";
};

singleton Material(nugget1)
{
   mapTo = "nugget1";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(ore3_base_stone3)
{
   mapTo = "base_stone3";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/OGA/mineassetexport/Texturi/clanta.png";
};

singleton Material(ore3_ore_vein3)
{
   mapTo = "ore_vein3";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
   glow[0] = "1";
};

singleton Material(caleferata_Lemn_001)
{
   mapTo = "Lemn_001";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(caleferata_metal_sina)
{
   mapTo = "metal_sina";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};
