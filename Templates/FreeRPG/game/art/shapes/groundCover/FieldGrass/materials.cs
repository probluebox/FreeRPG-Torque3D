
singleton Material(FieldGrass_GC_FieldGrass01)
{
   mapTo = "FieldGrass01";
   diffuseMap[0] = "FieldGrass01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   alphaTest = "1";
   alphaRef = "120";
   subSurface[0] = "1";
   subSurfaceColor[0] = "0.905882 1 0 1";
};
