
singleton Material(YelloPrarieFlow_GC_YelloPrarieLeaf)
{
   mapTo = "YelloPrarieLeaf";
   diffuseMap[0] = "YelloPrarie01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   subSurface[0] = "1";
   subSurfaceColor[0] = "0.760784 0.72549 0.266667 1";
   doubleSided = "1";
   alphaTest = "1";
   alphaRef = "120";
};
