
singleton Material(OfficeDivider_01_DividerWall)
{
   mapTo = "DividerWall";
   diffuseMap[0] = "3TD_Divider Wall_01_Blue";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = ".2";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
};

singleton Material(OfficeDivider_01_Divider_Frame)
{
   mapTo = "Divider_Frame";
   diffuseMap[0] = "3TD_DividerPaint_Gray_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = "0.2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
