
singleton Material(OfficeDivider_02_Divider02Wall)
{
   mapTo = "Divider02Wall";
   diffuseMap[0] = "3TD_Divider Wall_01_Blue";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = ".2";
   translucentBlendOp = "None";
};

singleton Material(OfficeDivider_02_Divider02_Frame)
{
   mapTo = "Divider02_Frame";
   diffuseMap[0] = "3TD_DividerPaint_Gray_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = ".2";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
};
