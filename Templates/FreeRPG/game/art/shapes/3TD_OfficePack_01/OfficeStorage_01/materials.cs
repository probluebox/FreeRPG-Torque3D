
singleton Material(OfficeStorage_01_OfficeStorageFrame)
{
   mapTo = "OfficeStorageFrame";
   diffuseMap[0] = "art/shapes/3TD_OfficePack_01/OfficeStorage_01/3TD_BlackLaminate";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OfficeStorage_01_OfficeStorageCover)
{
   mapTo = "OfficeStorageCover";
   diffuseMap[0] = "3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "150";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
