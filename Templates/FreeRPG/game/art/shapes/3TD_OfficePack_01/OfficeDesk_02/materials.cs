
singleton Material(OfficeDesk_02_BlackLaminateDesk2)
{
   mapTo = "BlackLaminateDesk2";
   diffuseMap[0] = "3TD_BlackLaminate";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = ".2";
   translucentBlendOp = "None";
};

singleton Material(OfficeDesk_02_WoodLaminateDesk2)
{
   mapTo = "WoodLaminateDesk2";
   diffuseMap[0] = "3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = ".2";
   translucentBlendOp = "None";
};
