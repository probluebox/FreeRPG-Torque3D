
singleton Material(OfficeFileCabinet_01_FileCabinetTrim)
{
   mapTo = "FileCabinetTrim";
   diffuseMap[0] = "3TD_OldDeskHandle_01";
   specular[0] = "0.996078 0.996078 0.996078 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = "5";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
};

singleton Material(OfficeFileCabinet_01_FileCabinetFrame)
{
   mapTo = "FileCabinetFrame";
   diffuseMap[0] = "3TD_DividerPaint_Dark_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
