
singleton Material(Desk_01_DeskWood_01_Horz)
{
   mapTo = "DeskWood_01_Horz";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "41";
   translucentBlendOp = "None";
   specularStrength[0] = "2.05882";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(Desk_01_DeskMetal)
{
   mapTo = "DeskMetal";
   diffuseMap[0] = "3TD_MetalBrass_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "3.33333";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(Desk_01_DeskWood_02_vert)
{
   mapTo = "DeskWood_02_vert";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "121";
   translucentBlendOp = "None";
   specularStrength[0] = "2.64706";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
