
singleton Material(ThePaintedWench_02Upload_DarkStone)
{
   mapTo = "DarkStone";
   diffuseMap[0] = "DarkStone";
   specular[1] = "White";
   translucentBlendOp = "None";
};

singleton Material(ThePaintedWench_02Upload_StoneWall)
{
   mapTo = "StoneWall";
   diffuseMap[0] = "StoneWall1";
   translucentBlendOp = "None";
};

singleton Material(ThePaintedWench_02Upload_Sign)
{
   mapTo = "Sign";
   diffuseMap[0] = "ThePaintedWenchSign.jpg.001";
   translucentBlendOp = "None";
};
